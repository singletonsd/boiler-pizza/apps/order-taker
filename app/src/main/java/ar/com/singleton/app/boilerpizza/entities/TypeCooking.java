package java.ar.com.singleton.app.boilerpizza.entities;

public enum  TypeCooking {
    NORMAL, WELL_DONE, BURNED, RAW
}
