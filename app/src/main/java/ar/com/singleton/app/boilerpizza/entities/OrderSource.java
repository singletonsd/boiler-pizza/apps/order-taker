package java.ar.com.singleton.app.boilerpizza.entities;

public enum OrderSource {
    CHAT_BOT, INSTAGRAM, FACEBOOK, SEBI, PATO, LUCAS, LOCALLY
}
