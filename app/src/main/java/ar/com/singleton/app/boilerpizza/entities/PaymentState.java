package java.ar.com.singleton.app.boilerpizza.entities;

public enum PaymentState {
    PAID, WAITING, UNPAID
}
