package java.ar.com.singleton.app.boilerpizza.entities;

import java.util.ArrayList;

public class Pizzas {
    private Integer id;
    private String name;
    private Float priceSale;
    private Float additionalCookingTime;
    private ArrayList<Ingredients> ingredients;
}
